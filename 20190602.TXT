###################################################################
# 2019/05/30 - PsychDOS 3.5" - Issues with adding software to the #
#                              installer                          #
###################################################################

It turns out that the lists for packages to install are stored in 
ISOLINUX/FDBOOT.IMG
           |--> FDSETUP\SETUP\FDPLBASE.LST
           |--> FDSETUP\SETUP\FDPLFULL.LST

However, adding paths to these files are not enough to add 3rd-party 
software because FreeDOS uses a tool during the install called 
"VFDUTIL.COM" (FDSETUP/BIN/VFDUTIL.COM). I'm going to comment-out lines 
51 through 54 of the file "FDSETUP\SETUP\FDINS600.BAT", which tells the 
script to 'GOTO AbortPkg' and then consequently to ':Done' if error
level 1. I did try running VFDUTIL.COM in DOSBox to see if there was a 
help message to better understand how it works, but it did not show 
anything and I have not been able to find any docs online.

Commenting out lines 51 through 54 of FDINS600.BAT may not be enough. 
It got to the 3rd-party package and threw an error for a brief moment 
and then went to a DOS console. It could be because there is no 
":PkgError" label.

I'm also commenting-out lines 31 through 35 and see if that helps.

The installation went through just fine; however, I'm getting errors on 
HD boot saying: "CONFIG.SYS error in line 2, 18, 19." I'm going to try 
only commenting-out line 34 and lines 51 through 54.

Okay, that seemed to have done the trick.

Nevermind; the installer did not throw an error but the 3rd-party 
package did not install. Maybe the 3rd-party package also needs to have 
a TXT file in PKGINFO like the others?

By the way, to mount a DOS IMG on GNU/Linux run:
    sudo mount -o loop,offset=32256 psychdos.img /folder

    * The "32256" comes from 512x63, which comes from
      'fdisk -l psychdos.img'

I need to see if a file called "PKG_FDI.LST" and "CLEANUP.LST" exists 
and the OS release info may be edited in a file called "VERSION.CFG". 
See: https://github.com/shidel/FDI/wiki/Creating-a-FDI-based-FreeDOS-
     installer#updating-your-application-packages

###############################################
# 2020/12/18 - PsychDOS 0.0.1 - The Workbench #
###############################################

I have basically decided to turn the dock at the bottom to focus on 
whatever the chosen Active File is. Right now, the dock has a 
"Change File" button for changing the Active File, a "Preview" button 
for viewing the or running the Active File, an "Edit" button for editing
the Active File, a "Convert/Compile" button for converting or compiling 
(*.BAS) the Active File, and a few other options. Of course,at the 
moment, some of this is not set in stone, but I believe it will be 
incredibly helpful in saving time from having to constantly navigate to 
the same file that can be edited with multiple programs. Maybe renaming 
the "Dock" to "Workbench" would make more sense at this point. I am also
removing the GMENU-related button from the dock/workbench and giving 
each category in the Programs menu a "Custom" option in which people can
use the Launch/Edit options to add and run their own software. Custom 
needs its own special "&" (Alt+?) shortcut. 

[2021/01/18 Update]: The Convert/Compile button was replaced by a simple
                     "Clear" button for the Active File as this proved 
                     to be a bit more difficult than I though; however,
                     if applicable, certain menu items that require a 
                     filepath as an argument can still be used for 
                     compiling/converting. Also, "Custom" was changed to
                     "Cust0m" since it is far more unlikely a program
                     will start or even contain a "0".

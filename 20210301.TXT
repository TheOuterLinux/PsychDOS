###########################################################
# 2021/03/01 - PsychDOS 0.0.1 - Adding code back to Timer #
###########################################################

So after removing lots of code from Timer_PsychDOS, I had to add some of
it back so that certain items in the Programs menu would enable/disable 
based on the current Active File's extension or the lack there of a file
at all since some of the Program items are command-line only.

#########################################################
# 2021/01/07 - PsychDOS 0.0.1 - RUNME.EXE (PRGMNGR.EXE) #
#########################################################

[2021/01/19 Update]: The RUNME.EXE file was renamed to PRGMNGR.EXE after
                     realizing that I needed to have people run a 
                     RUNME.BAT file to start things instead if I also
                     wanted to SET PATH=%PATH%;.... and so forth.

I have been adding more software to the Programs menu and improving the 
Workbench. I have also been working on the lock screen and screensaver. 
Also, to save RAM, I created a RUNME.EXE that you initiate PsychDOS with
rather than the PSYCHDOS.EXE. What this does is it uses SHELL to start
PsychDOS. When a program is clicked, the path of that program (and file 
if applicable) is stored to "CONFIG\LASTRUN.CFG". Afterwards, 
PSYCHDOS.EXE is told to END and then the RUNME.EXE opens the 
"CONFIG\LASTRUN.CFG" and the "CONFIG\DOCKCMBO.CFG" file to see what 
program to run and how to run it. After you quit the program, the 
"CONFIG\LOWRAM.CFG" file is set to "FALSE" and PSYCHDOS.EXE is told to 
start again, all while in a WHILE-LOOP that ends as long as you exit 
PsychDOS the normal way. There may be a few more complex stuff added in 
the future, but hopefully all of that made sense. You are basically 
running software on top of the RUNME.EXE instead of PSYCHDOS.EXE, which 
uses hardly and resources.

##############################################
# 2020/12/16 - PsychDOS 0.0.1 - No eye-candy #
##############################################

I made some good progress on the PsychDOS desktop environment. Each 
Programs category, as well as the dock now have a menu item for helping 
to add more software via GMENU. I have also been working on a file 
chooser for if a command-line program needs one such as MPlayer, even 
though DN151 file manager should be able to handle files based on
extension. I'm also trying my best to not require a mouse while putting 
this together. As for as any fancy "eye-candy" goes, if there is to be 
"decoration," the only way that I know of right now is to use labels 
like pixels, which is kind of ridiculous and would slow things waaay 
down.

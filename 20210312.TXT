##########################################################
# 2021/03/12 - PsychDOS 0.0.1 - Pastebin functions added #
##########################################################

I managed to replace PsychDOS installed on the image (*.img) with 
FreeDOS with a newer version that includes functioning pastebin cURL 
information and so far, so good except for the "ix.io" button; the error
has something to do with the "F:1" part of the cURL script but should be
fixable.

Update 2021/03/17: The issue with the "F:1" was fixed by changing it to 
                   "f:1".

#################################################
# 2019/05/29 - PsychDOS 3.5" - FreeDOS and QEMU #
#################################################

Create IMG to store installed OS with QEMU:

    qemu-img create psychos.img 1G

Boot PsychDOS ISO and use psychdos.img as hard drive:

  qemu -hda psychdos.img -cdrom psychdos.iso -boot d -m 256

  * -boot d = boot off of cdrom
  * -m 256 = 256 MB of RAM

After installing PsychDOS, run only the IMG in QEMU to test:

  qemu -hda psychdos.img -m 256

I need to figure out how FDIMPLES works so I can have it install to a 
custom directory.

I wonder if I can just setup everything in an IMG and then use 
UNetbootin to make it bootable? Then, create an ISO from that...

  - Do a full install of FreeDOS
  - Customize FDIMPLES to install items to other folders.
  - Install all items in FDIMPLES and add files to original fdos.img
  - Repack original FreeDOS ISO
  - The file size will be much larger doing this, but it will have more 
    of an out-of-box experience and will be able to recover programs if 
    lost or deleted.
